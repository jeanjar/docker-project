#!/usr/bin/env bash
chmod 777 -R /apps/lpc-portal/public/uploads
chmod 777 -R /apps/lpc-portal/storage/logs
chmod 777 -R /apps/lpc-portal/storage/framework
