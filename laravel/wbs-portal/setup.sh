#!/usr/bin/env bash
chmod 777 -R /apps/wbs-portal/public/uploads
chmod 777 -R /apps/wbs-portal/storage/logs
chmod 777 -R /apps/wbs-portal/storage/framework